#[macro_use]
extern crate log;

pub mod xml;
pub mod http;
pub mod fs;
