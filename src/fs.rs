use std::convert::AsRef;
use std::ffi::OsStr;
use std::fs::{File, create_dir_all, remove_dir_all, remove_file};
use std::io::{Read, Error as IoError, Result as IoResult, Write};
use std::path::{Path, PathBuf};

use rand::random;

/// Creates a temporary directory context w/ files since my tmp dir perms are goobed
pub struct TempFileManager {
    dir: PathBuf,
    last_fname: usize
}

impl TempFileManager {
    pub fn new(dir: &str) -> TempFileManager {
        let mut tmpdir = PathBuf::from(dir);
        tmpdir.push(Path::new(format!("tmpdir_{}", random::<u64>()).as_str()));
        create_dir_all(&tmpdir).expect("Unable to create tmpdir");
        TempFileManager {
            dir: tmpdir,
            last_fname: 0,
        }
    }

    pub fn new_file(&mut self) -> IoResult<File> {
        File::create(self.new_path())
    }

    pub fn new_path(&mut self) -> PathBuf {
        self.last_fname += 1;
        self.dir.join(
            Path::new(format!("{}", &self.last_fname).as_str())
        )
    }

    pub fn dirname(&self) -> &Path {
        self.dir.as_path()
    }
}

impl Drop for TempFileManager {
    fn drop(&mut self) {
        remove_dir_all(self.dirname()).expect("Unable to rm tmp dir");
    }
}

pub struct TempPath {
    inner: PathBuf
}

impl TempPath {
    pub fn new(parent_dir: Option<&str>) -> TempPath {
        TempPath {
            inner: PathBuf::from(parent_dir.unwrap_or("./")).join(Path::new(format!("tmp_{}", random::<u64>()).as_str()))
        }
    }

    pub fn to_str(&self) -> Option<&str> {
        self.inner.to_str()
    }
}

impl AsRef<Path> for TempPath {
    fn as_ref(&self) -> &Path { &self.inner }
}

impl AsRef<OsStr> for TempPath {
    fn as_ref(&self) -> &OsStr { &self.inner.as_ref() }
}


impl Drop for TempPath {
    fn drop(&mut self) {
        if self.inner.exists() {
            remove_file(&self.inner).expect("Unable to remove temporary file")
        }
    }
}

pub struct TempFile {
    path: TempPath,
    inner: File
}

impl TempFile {
    pub fn new(base_dir: Option<&str>) -> Result<TempFile, IoError> {
        let tp = TempPath::new(base_dir);
        match File::create(&tp) {
            Ok(f) => Ok(
                TempFile {
                    path: tp,
                    inner: f
                }
            ),
            Err(e) => Err(e)
        }
    }

    pub fn path(&self) -> &TempPath {
        &self.path
    }
}

impl Write for TempFile {
    fn write(&mut self, buf: &[u8]) -> std::result::Result<usize, IoError> { self.inner.write(buf) }
    fn flush(&mut self) -> std::result::Result<(), IoError> { self.inner.flush() }
}

impl Read for TempFile {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize, IoError> { self.inner.read(buf) }
}


#[cfg(test)]
mod tests {
    // Must run in single threaded mode
    use std::fs::{create_dir_all, read_dir, remove_dir};
    use std::io::Result as IoResult;
    use std::path::PathBuf;

    use crate::fs::TempFileManager;

    fn setup_test_container(container_name: &str) -> IoResult<()> {
        create_dir_all(PathBuf::from(container_name))
    }

    fn dirlist(p: &str) -> Vec<PathBuf> {
        read_dir(p).expect("not read").map(|f| f.expect("not f").path()).collect()
    }

    #[test]
    fn dir_rmd() {
        let test_container_dir = "./vvvv";
        setup_test_container(&test_container_dir).expect("not dir creat");
        let files_pre: Vec<PathBuf> = dirlist(test_container_dir);
        let f = TempFileManager::new(test_container_dir);
        drop(f);
        let files_after: Vec<PathBuf> = dirlist(test_container_dir);
        assert_eq!(files_pre, files_after);
        remove_dir("./vvvv").unwrap();
    }

    #[test]
    fn f_create() {
        let test_container_dir = "./";
        setup_test_container(&test_container_dir).expect("not setup");
        let mut f = TempFileManager::new(test_container_dir);
        let num_fs: usize = 10;
        for _ in 0..num_fs {
             f.new_file().expect("Unable to create file");
        }
        assert_eq!(num_fs, dirlist(f.dirname().to_str().expect("not str")).len());
        drop(f);
    }
}
