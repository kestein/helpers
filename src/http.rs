use std::collections::HashSet;
use std::fs::File;
use std::io::Write;
use std::iter::FromIterator;
use std::path::{Path, PathBuf};

use hyper::body::Bytes;
use rand::{
    random,
    Rng, SeedableRng,
    rngs::StdRng
};
use reqwest::{
    Client, ClientBuilder, Error, RequestBuilder, Response, Url,
    header::CONTENT_TYPE,
    redirect::Policy
};
use tokio::time::{Duration, delay_for};
use serde::ser::Serialize;
use serde_json;

pub const CURRENT_USER_AGENT: &str = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36";
const MAX_RETRIES: usize = 5;


fn redirect_logic() -> Policy {
    Policy::custom(|attempt| {
        let num_redirects = attempt.previous().len();
        let follow_strs: Vec<String> = attempt.previous().iter().map(|u| format!("{}", u)).collect::<Vec<String>>();
        let uniq_redirs: HashSet<&String> = HashSet::from_iter(&follow_strs);
        if uniq_redirs.len() < num_redirects {
            return attempt.error("Redirect loop detected")
        }
        if attempt.previous().len() > 30 {
            attempt.error(
                format!("Too many redirects {}", follow_strs.join(","))
            )
        } else {
            if num_redirects > 1 {
                info!("Redirects: {}", follow_strs.join(","));
            }
            attempt.follow()
        }
    })
}

#[cfg(target_os="windows")]
fn base_client() -> Client {
    ClientBuilder::new()
        .gzip(true)
        .use_rustls_tls()
        .user_agent(CURRENT_USER_AGENT)
        .redirect(redirect_logic())
        .build().expect("Unable to build client")
}

#[cfg(not(target_os="windows"))]
fn base_client() -> Client {
    ClientBuilder::new()
        .gzip(true)
        .user_agent(CURRENT_USER_AGENT)
        .redirect(redirect_logic())
        .build().expect("Unable to build client")
}

pub fn base_get_client(url: &str) -> RequestBuilder {
    base_client().get(Url::parse(url).expect("Invalid url"))
}

pub fn base_post_client(url: &str) -> RequestBuilder {
    base_client().post(Url::parse(url).expect("Invalid url"))
}

fn create_get_client(url: &str, cookie: &str) -> RequestBuilder {
    base_get_client(url)
        .header("DNT", "1")
        // This one is worth experimenting with at some point
        // .header("Cache-Control", "no-cache")
        .header("Connection", "keep-alive")
        .header("Upgrade-Insecure-Requests", "1")
        .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
        .header("Cookie", cookie)
}

async fn sleep(min_ms: u64, max_ms: u64) {
    assert!(max_ms > 1000, "max_ms parameter must be at least 1 second");
    let mut time_frame = StdRng::seed_from_u64(random::<u64>()).gen_range(min_ms, max_ms);
    if time_frame < 1000 {
        time_frame += 1000;
    }
    delay_for(
        Duration::from_millis(time_frame)
    ).await;
}

pub async fn make_request(client: RequestBuilder) -> Response {
    let mut tries = 0;
    let mut final_resp: Option<Response> = None;
    while let None = final_resp {
        // TOOD: Move to while let once https://github.com/rust-lang/rust/issues/53667 is finished (if ever)
        if tries > MAX_RETRIES {
            break;
        }
        if tries > 0 {
            info!("{}", format!("Retry {}", tries));
        }
        // Increment before the request such that an error response can be processed by the consumer
        tries += 1;
        let r: Result<Response, Error> = client.try_clone().expect("Unable to clone RequestBuilder").send().await;
        match r {
            Ok(resp) => {
                match resp.status().as_u16() {
                    200..=299 => {
                        sleep(1000, 5000).await;
                        final_resp = Some(resp);
                        break;
                    },
                    300..=399 => {
                        info!("Redirect status code: {}", resp.status());
                        info!("Headers");
                        for (key, value) in resp.headers() {
                            match value.to_str() {
                                Ok(s) => { info!("{} {}", key, s); },
                                Err(_) => { info!("{} {:?}", key, value.as_bytes()); }
                            }
                        }
                        info!("Cookies");
                        for c in resp.cookies() {
                            info!("{:?}", c);
                        }
                        unimplemented!("Still working on redirect logic");
                    },
                    401 => {
                        error!("Authorization error, refresh cookie");
                        match resp.error_for_status() {
                            Ok(_) => { panic!("Bad thing") },
                            Err(e) => {
                                error!("Encountered 4XX level code");
                                panic!(format!("{}", e));
                            }
                        }
                    }
                    400 | 402..=499 => {
                        match resp.error_for_status() {
                            Ok(_) => { panic!("Bad thing") },
                            Err(e) => {
                                error!("Encountered 4XX level code");
                                panic!(format!("{}", e));
                            }
                        }
                    },
                    500..=599 => {
                        if tries < MAX_RETRIES {
                            warn!("Encountered server error. Long sleeping before retrying");
                            sleep(5000, 15000).await;
                        }
                    }
                    code @ _ => {
                        panic!(format!("Unknown response code {}", code));
                    }
                };
                if tries > MAX_RETRIES {
                    final_resp = Some(resp);
                }
            },
            Err(e) => {
                // First time going directly going to a page causes a mass redirect chain back to the front page. Try the request directly again.
                if e.is_redirect() {
                    sleep(1000, 5000).await;
                } else {
                    panic!(format!("{}", e))
                }
            }
        }
    }
    final_resp.expect("Max retries exceeded")
}

pub async fn download_image(url: &str, cookie: &str, file_path_no_ext: &PathBuf) -> Bytes {
    let resp = make_request(create_get_client(url, cookie)).await;
    let ftype;
    {
        let headers = resp.headers();
        let f = headers[CONTENT_TYPE].to_str().unwrap().to_string();
        if f.starts_with("image/") || f.starts_with("video/") {
            ftype = String::from(&f[6..]);
        } else {
            ftype = f;
        }
    }
    let image = resp.bytes().await.expect("Unable to bytes request");
    let mut f = File::create(file_path_no_ext.with_extension(ftype)).unwrap();
    f.write_all(&image.clone()[..]).unwrap();
    image
}

pub async fn download_html(url: &str, cookie: &str, output_file: &impl AsRef<Path>) -> String {
    let mut tries: usize = 0;
    let mut resp_text: Option<String> = None;
    while let None = resp_text {
        // TOOD: Move to while let once https://github.com/rust-lang/rust/issues/53667 is finished (if ever)
        if tries > MAX_RETRIES {
            error!("Exceeded max number of retries to dl html");
            break;
        }
        tries += 1;
        match make_request(create_get_client(url, cookie)).await.text().await {
            Ok(data) => {
                let mut f = File::create(output_file).unwrap();
                f.write_all(data.clone().as_bytes()).unwrap();
                resp_text = Some(data);
            },
            Err(e) => {
                if tries > MAX_RETRIES || !e.is_body() {
                    panic!(format!("{}", e));
                }
            }
        }
    }
    resp_text.unwrap()
}

pub async fn download_bytes(url: &str, output_file: &impl AsRef<Path>) {
    let resp = make_request(create_get_client(url, "")).await;
    File::create(output_file)
        .expect("Unable to create output file")
        .write_all(
            &resp.bytes()
                .await
                .expect("Could not retrieve bytes")
                .slice(..)
        ).expect("Unable to write bytes");
}

pub async fn post_and_get_json(url: &str, json: &impl Serialize) -> serde_json::Value {
    make_request(
        base_post_client(url)
            .json(json)
    )
    .await
    .json().await.expect("Unable to deserialize json response")
}
