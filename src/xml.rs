use quick_xml::events::attributes::Attribute;

/// Breaks out an XML event attribute into a tuple of Strings
///
/// param: XML event attribute
/// return: (Key, value)s
pub fn attribute_to_tuple(attr: Attribute) -> (String, String) {
    (
        String::from_utf8(Vec::from(attr.key)).expect("key bad"),
        match attr.unescaped_value() {
            Ok(u) => {
                String::from_utf8(u.into_owned()).expect("Value bad")
            },
            Err(e) => {
                error!("{:?} {:?}", attr, e);
                panic!("Error parsing value");
            }
        }
    )
}
